import React, { Component } from "react";
import { glassesArr } from "../Data/Data";
import Item from "./Item";
import Layout from "./Layout";

export default class RenderGlasses extends Component {
  state = {
    listGlasses: glassesArr,
    glassesOj: {},
  };

  RenderListGlasses = () => {
    return this.state.listGlasses.map((item) => {
      return (
        <Item
          key={item.id}
          glass={item}
          onClick={() => {
            this.handleGlass(item);
          }}
        />
      );
    });
  };
  handleGlass = (cx) => {
    this.setState({ glassesOj: cx });
  };
  render() {
    return (
      <div>
        <Layout glassesOj={this.state.glassesOj} />
        <div className="row">{this.RenderListGlasses()}</div>
      </div>
    );
  }
}
