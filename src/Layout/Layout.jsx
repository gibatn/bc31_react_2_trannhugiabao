import React, { Component } from "react";
import { glassesArr } from "../Data/Data";
import Item from "./Item";
import RenderGlasses from "./RenderGlasses";
export default class Layout extends Component {
  render() {
    let { url, name, desc } = this.props.glassesOj;

    return (
      <div
        className="card position-relative mx-auto"
        style={{ width: "22rem" }}
      >
        {this.props.glassesOj.url !== undefined && (
          <img
            className="card-img-top position-absolute w-50"
            style={{ top: "100px", right: "85px" }}
            src={url}
            alt="Card image cap"
          />
        )}
        <div className="card-body">
          <img
            className="card-img-top"
            src="./glassesImage/model.jpg"
            alt="Card image cap"
          />
          <div
            className="position-absolute bg-warning"
            style={{ top: "300px", minHeight: "120px" }}
          >
            <h5 className="card-title text-center text-danger">{name}</h5>
            <p className="card-text">
              {this.props.glassesOj.url !== undefined && desc.length > 50
                ? desc.slice(0, 50) + "..."
                : desc}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
