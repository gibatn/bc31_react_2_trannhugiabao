import React, { Component } from "react";
import Layout from "./Layout";

export default class Item extends Component {
  render() {
    return (
      <div className="col-3 pt-4">
        <img
          onClick={this.props.onClick}
          className="w-50"
          src={this.props.glass.url}
          alt="Card image cap"
        />
      </div>
    );
  }
}
